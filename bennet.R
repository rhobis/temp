#' Bennett Algorithm 
#' 
#' Bennett algorithm is an adaptive algorithm proposed by Fattorini (2009) for 
#' estimating inclusion probabilities through Monte Carlo method.
#' 
#' @param y observations of the target variable in the sample
#' @param x auxiliary variable values for the population
#' @param sample_labels numeric, a vector with the indices of sampled units
#' @param design sampling procedure to be used for sample selection. Either a string 
#' indicating the name of the sampling design or a function; see section "Details" for more information.
#' @param L number of Monte Carlo iterations to perform at each step of the algorithm
#' @param K minimum acceptable numbere of consecutive steps that satisfy the exit conditions
#' @param delta maximum acceptable error
#' @param eps numeric value between 0 and 1
#' @param alpha numeric value between 0 and 1
#' @param design_pars only used when a function is passed to argument design, 
#' named list of parameters to pass to the sampling design function.
#' 
#' 
#' 
#' @details 
#' Argument design accepts either a string indicating the sampling design to use 
#' to draw samples or a function. Accepted designs are "brewer", "tille", 
#' "maxEntropy", "poisson", "sampford", "systematic", "randomSystematic". 
#' The user may also pass a function as argument; such function should take as 
#' input the parameters passed to argument design_pars and return either a logical 
#' vector or a vector of 0s and 1s, where TRUE or 1 indicate sampled units and FALSE 
#' or 0 indicate non-sample units. The length of such vector must be equal to the 
#' length of x if units is not specified, otherwise it must have the same length of units.
#' 
#' 
#' 



bennett <- function(y, x, sample_labels, design, design_pars=NULL, K=2, L=1000, 
                   delta=0.001, eps=0.1, alpha=0.1 ){
    
    ### Input check ---
    if( missing(design_pars) ) stop('Missing argument: design_pars')
    
    ### Initialisation ---
    n <- length(y)
    k <- 0
    i <- 0
    P <- 100
    M <- Inf
    occurrences <- matrix(1, n, n)
    ce   <- (eps/(2+2*eps) + 1) * log( eps/(2+2*eps) + 1 ) - eps/(2+2*eps)
    
    ### Bennet algorithm ---
    while( k<K | L*i<M | P>alpha ){
        
        i    <- i+1        
        
        suppressMessages(
            occurrences <- (occurrences - 1) +
                (L+1)*jipApprox::jip_MonteCarlo(x, n, replications = L, design, sample_labels, 
                                     seed = NULL, as_data_frame = FALSE, design_pars,
                                     progress_bar = FALSE)
        )
        pikl <- occurrences/(L*i+1)
        pik  <- diag(pikl)
        ht   <- drop(crossprod(y, 1/pik)) # Horvitz-Thompson total estimator
        
        yy   <- outer(y,y)
        pp   <- 1/outer(pik, pik) - 1/pikl
        vv   <- sum(yy*pp)                # Horvitz-Thompson variance estimator
        
        pi0  <- min(pik)
        M    <- 16*log(2/alpha)*eps^(-2)/(pi0)
        P    <- 2 * sum( exp(-ce * M * pik))
        
        if( i>1 ){
            unbiasedness <- abs(ht - ht0)/ht0 < delta 
            precision    <- abs(vv - vv0)/vv0 < delta
            k <- ifelse( unbiasedness & precision, k+1, 0)
        }
        ht0 <- ht
        vv0 <- vv
    }
    
    ### Return results ---
    return( 
        list( 
            parameters = list(
                L = L,
                delta = delta,
                eps = eps,
                alpha = alpha,
                K = k
            ),
            output = list(
                steps = i,
                P = P,
                k = k,
                tot = ht,
                var = vv,
                probs = pikl
            )
        )
    )
}